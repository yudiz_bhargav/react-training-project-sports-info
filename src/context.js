import React, { useState, useEffect, useContext } from 'react';

import axios from 'axios';

const url = 'https://backend.sports.info/api/v1/posts/recent';

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [sports, setSports] = useState([]);
  const [isAuth, setIsAuth] = useState(false);

  const [postsPerPage, setPostsPerPage] = useState(2);
  const [currentPage, setCurrentPage] = useState(1);

  const [nStart, setNStart] = useState(0);
  const [nLimit, setNLimit] = useState(2);

  // Get current Sports Articles for Pagination
  const indexOfLastSport = currentPage * postsPerPage;
  const indexOfFirstSport = indexOfLastSport - postsPerPage;
  const currentSports = sports.slice(indexOfFirstSport, indexOfLastSport);
  const totalPosts = 20;

  // Change Page
  const paginate = (pageNumber) => {
    localStorage.setItem('page-number', pageNumber);
    const currentPageNumber = localStorage.getItem('page-number');

    setNStart(currentPageNumber);
    console.log(nStart);
    setCurrentPage(currentPageNumber);
  };

  const fetchSports = async () => {
    setLoading(true);
    try {
      const response = await axios
        .post(url, {
          nStart: nStart,
          nLimit: nLimit,
          eSort: 'Latest',
          bRemoveBannerPosts: true,
        })
        .then((response) => response.data.data)
        .then((data) => {
          const newSports = data.map((sport) => {
            const {
              _id,
              eType,
              sTitle,
              sSlug,
              sImage,
              nCommentsCount,
              sDescription,
              nViewCounts,
              dUpdatedAt,
              dCreatedAt,
            } = sport;
            return {
              id: _id,
              type: eType,
              title: sTitle,
              slug: sSlug,
              image: sImage,
              comments: nCommentsCount,
              description: sDescription,
              views: nViewCounts,
              updated: dUpdatedAt,
              created: dCreatedAt,
            };
          });
          setSports(newSports);
        })
        .catch((error) => console.error(error));

      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchSports();
  }, [currentPage]);

  return (
    <AppContext.Provider
      value={{
        loading,
        isAuth,
        sports,
        setIsAuth,
        currentSports,
        postsPerPage,
        totalPosts,
        paginate,

        setNStart,
        setNLimit,
        setSports,
        setLoading,
        nStart,
        nLimit,
        setCurrentPage,
        currentPage,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppContext, AppProvider };
