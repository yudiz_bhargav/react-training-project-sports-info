import React from 'react';

const ProtectLogin = () => {
  if (localStorage.getItem('loggedIn') === 'yes') {
    return history.push('./home/1');
  }
};

export default ProtectLogin;
