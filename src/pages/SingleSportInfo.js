import React, { useState, useEffect } from 'react';
import Loading from '../components/Loading';
import { useParams, Link, withRouter } from 'react-router-dom';

import Navbar from '../components/Navbar';

const url = 'https://backend.sports.info/api/v1/posts/recent';

const SingleSportsInfo = () => {
  const { id } = useParams();
  const [loading, setLoading] = useState(false);
  const [sportArticle, setSportArticle] = useState(null);

  // const { sports, loading } = useGlobalContext();

  useEffect(() => {
    setLoading(true);
    async function getSportsArticles() {
      try {
        const response = await fetch(url);
        const resData = await response.json();
        const { data } = resData;
        if (data) {
          const newSports = data.map((sport) => {
            const {
              _id,
              eType,
              sTitle,
              sSlug,
              sImage,
              nCommentsCount,
              sDescription,
              nViewCounts,
              dUpdatedAt,
              dCreatedAt,
            } = sport;
            return {
              id: _id,
              type: eType,
              title: sTitle,
              slug: sSlug,
              image: sImage,
              comments: nCommentsCount,
              description: sDescription,
              views: nViewCounts,
              updated: dUpdatedAt,
              created: dCreatedAt,
            };
          });
          const selectedSportsArticle = newSports.filter(
            (sport) => sport.id === id
          )[0];
          setSportArticle(selectedSportsArticle);
        } else {
          setSportArticle(null);
        }

        setLoading(false);
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    }
    getSportsArticles();
  }, [id]);

  if (loading) {
    return (
      <>
        <Navbar />
        <Loading />
      </>
    );
  }

  if (!sportArticle) {
    return (
      <>
        <Navbar />
        <h2 className='section-title'>no article to display</h2>
      </>
    );
  }

  const {
    comments,
    created,
    description,
    image,
    // slug,
    title,
    // type,
    updated,
    views,
  } = sportArticle;

  return (
    <>
      <Navbar />
      <section className='section cocktail-section'>
        <Link to='/home/1' className='btn btn-primary'>
          back home
        </Link>
        <h2 className='section-title'>{title}</h2>
        <div className='drink'>
          <img src={image} alt='single sport' />
          <div className='drink-info'>
            <p>
              <span className='drink-data'>Description:</span>
              {description}
            </p>
            <p>
              <span className='drink-data'>Comments:</span>
              {comments}
            </p>
            <p>
              <span className='drink-data'>Views:</span>
              {views}
            </p>
            <p>
              <span className='drink-data'>Created on:</span>
              {created}
            </p>
            <p>
              <span className='drink-data'>Updated on:</span>
              {updated}
            </p>
          </div>
        </div>
      </section>
    </>
  );
};

export default withRouter(SingleSportsInfo);
