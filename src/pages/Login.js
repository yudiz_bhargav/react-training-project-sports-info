import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useGlobalContext } from '../context';
import Alert from '../functions/Alert';

const Login = () => {
  const [userEmail, setUserEmail] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [alert, setAlert] = useState({
    show: false,
    msg: '',
    type: '',
  });

  const { /* isAuth, */ setIsAuth } = useGlobalContext();

  const email = 'abc@xyz.com';
  const password = '123';

  const history = useHistory();

  const showAlert = (show = false, type = '', msg = '') => {
    setAlert({ show, type, msg });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (userEmail === email && userPassword === password) {
      showAlert(true, 'success', 'Logged In!');
      setIsAuth(true);
      localStorage.setItem('loggedIn', 'yes');
      console.log('yes authenticated');
      history.push('/home/1');
    } else {
      setIsAuth(false);
      console.log('not authenticated');
      showAlert(true, 'danger', 'please ONLY enter placeholder values');
    }
  };

  if (/* isAuth */ localStorage.getItem('loggedIn') === 'yes') {
    return history.push('./home/1');
  }

  return (
    <section className='section-center'>
      <form action='' className='login-form' onSubmit={submitHandler}>
        {alert.show && (
          <Alert {...alert} removeAlert={showAlert} className='alert' />
        )}
        <h2>Login Page</h2>
        <label htmlFor='email' className='labels'>
          Email:
        </label>
        <input
          type='text'
          className='inputs'
          id='email-or-mobile'
          placeholder='abc@xyz.com'
          onChange={(e) => {
            setUserEmail(e.target.value);
          }}
        />
        <label htmlFor='password' className='labels'>
          Password:
        </label>
        <input
          type='password'
          className='inputs'
          id='password'
          placeholder='123'
          onChange={(e) => {
            setUserPassword(e.target.value);
          }}
        />
        <button type='submit-btn' className='submit-btn'>
          Login
        </button>
      </form>
    </section>
  );
};

export default Login;
