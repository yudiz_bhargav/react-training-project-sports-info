import React from 'react';
import SportsCardList from '../components/SportsCardList';

import Pagination from '../components/Pagination';

import Navbar from '../components/Navbar';

import { withRouter } from 'react-router';

const Home = () => {
  return (
    <main>
      <Navbar />
      <SportsCardList />
      <Pagination />
    </main>
  );
};

export default withRouter(Home);
