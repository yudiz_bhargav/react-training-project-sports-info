import React from 'react';
import { Link } from 'react-router-dom';

const Error = () => {
  return (
    <section className='error-page section'>
      <div className='error-container'>
        <h1>Page you are trying to visit does not exist!</h1>
        <Link to='/home/1' className='btn btn-primary'>
          Back Home
        </Link>
      </div>
    </section>
  );
};

export default Error;
