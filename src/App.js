import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useGlobalContext } from './context';

// importing pages
import Login from './pages/Login';
import Home from './pages/Home';
import Error from './pages/Error';
import SingleSportInfo from './pages/SingleSportInfo';

import ProtectedRoute from './ProtectedRoute';

function App() {
  const { isAuth } = useGlobalContext();

  return (
    <div>
      <Router>
        <Switch>
          <Login exact path='/' />
          <ProtectedRoute path='/home/:page' component={Home} isAuth={isAuth} />
          <ProtectedRoute
            path='/single-sport/:id'
            component={SingleSportInfo}
            isAuth={isAuth}
          />
          <Route path='*'>
            <Error />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
