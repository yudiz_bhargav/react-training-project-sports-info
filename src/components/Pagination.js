import React from 'react';

import { useGlobalContext } from '../context';
import { Link } from 'react-router-dom';

const Pagination = () => {
  const { postsPerPage, totalPosts, paginate } = useGlobalContext();
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav>
      <ul className='pagination'>
        {pageNumbers.map((number) => (
          <li key={number} className='page-item'>
            <Link
              to={`/home/${number}`}
              className='page-link'
              onClick={() => paginate(number)}
            >
              {number}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;
