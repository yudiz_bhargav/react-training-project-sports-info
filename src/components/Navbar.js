import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className='navbar'>
      <div className='nav-center'>
        <Link to='/home/1'>
          <h1 className='logo'>
            <span>Cricket</span>Fever
          </h1>
        </Link>
        <ul className='nav-links'>
          <li>
            <Link to='/home/1'>Home</Link>
          </li>
          <li>
            <Link
              to='/'
              onClick={() => {
                localStorage.setItem('loggedIn', 'no');
              }}
            >
              Logout
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
