import React from 'react';
import { Link } from 'react-router-dom';

const SportsCard = ({ id, title, image, comments, views }) => {
  return (
    <article className='cocktail'>
      <div className='img-container'>
        <img src={image} alt='sports' />
      </div>
      <div className='cocktail-footer'>
        <h3>{title}</h3>
        <h4>Views: {views}</h4>
        <p>Comments: {comments}</p>
        <Link to={`/single-sport/${id}`} className='btn btn-primary'>
          view article
        </Link>
      </div>
    </article>
  );
};

export default SportsCard;
