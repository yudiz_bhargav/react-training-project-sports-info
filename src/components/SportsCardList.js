import React, { useState, useEffect } from 'react';
import SportsCard from './SportsCard';
import Loading from './Loading';
// import Pagination from './Pagination';
import { useGlobalContext } from '../context';
import { useParams } from 'react-router';

import axios from 'axios';

const url = 'https://backend.sports.info/api/v1/posts/recent';

const SportsCardList = () => {
  const { page } = useParams();
  localStorage.setItem('page-number', page);
  // const [loading, setLoading] = useState(false);
  // const [postsPerPage, setPostsPerPage] = useState(2);
  // const [currentPage, setCurrentPage] = useState(1);
  // const [sports, setSports] = useState([]);

  const {
    sports,
    setSports,
    loading,
    currentSports,
    setNStart,
    setNLimit,
    setLoading,
    nStart,
    nLimit,
    // setCurrentPage,
    currentPage,
  } = useGlobalContext();

  // const fetchSports = async () => {
  //   setNStart(2);
  //   setLoading(true);
  //   // setCurrentPage(page);
  //   try {
  //     const response = await axios
  //       .post(url, {
  //         nStart: nStart,
  //         nLimit: nLimit,
  //         eSort: 'Latest',
  //         bRemoveBannerPosts: true,
  //       })
  //       .then((response) => response.data.data)
  //       .then((data) => {
  //         const newSports = data.map((sport) => {
  //           const {
  //             _id,
  //             eType,
  //             sTitle,
  //             sSlug,
  //             sImage,
  //             nCommentsCount,
  //             sDescription,
  //             nViewCounts,
  //             dUpdatedAt,
  //             dCreatedAt,
  //           } = sport;
  //           return {
  //             id: _id,
  //             type: eType,
  //             title: sTitle,
  //             slug: sSlug,
  //             image: sImage,
  //             comments: nCommentsCount,
  //             description: sDescription,
  //             views: nViewCounts,
  //             updated: dUpdatedAt,
  //             created: dCreatedAt,
  //           };
  //         });
  //         setSports(newSports);
  //       })
  //       .catch((error) => console.error(error));

  //     setLoading(false);
  //   } catch (error) {
  //     console.log(error);
  //     setLoading(false);
  //   }
  // };

  // useEffect(() => {
  //   fetchSports();
  //   console.log(currentPage);
  // }, [currentPage]);

  if (loading) {
    return <Loading />;
  }
  if (currentSports.length < 0) {
    return <h2 className='section-title'>no sports info was found</h2>;
  }

  return (
    <section>
      <h2 className='section-title'>Sports Articles</h2>
      <div className='cocktails-center'>
        {sports.map((item) => {
          return <SportsCard key={item.id} {...item} />;
        })}
      </div>
    </section>
  );
};

export default SportsCardList;
